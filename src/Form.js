import React, { Component } from 'react';

class Form extends Component {
  constructor (props) {
    super(props);

    this.state = {
      email: '',
      password: '',
      formErrors: {
        email: '', 
        password: ''
      },
      emailValid: 0,
      passwordValid: 0,
      formValid: 0
    }
  }
  
  handleUserInput = (e) => {
    const name = e.target.name;
    const value = e.target.value;

    this.setState(
      {[name]: value},
      () => { this.validateField(name, value) }
    );
  }
  
  validateField(fieldName, value) {
    let fieldValidationErrors = this.state.formErrors;
    let emailValid = this.state.emailValid;
    let passwordValid = this.state.passwordValid;

    switch(fieldName) {
      case 'email':
        emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
        fieldValidationErrors.email = emailValid ? '' : ' is invalid';
        break;
      case 'password':
        passwordValid = value.length >= 6;
        fieldValidationErrors.password = passwordValid ? '': ' is too short';
        break;
      default:
        break;
    }

    this.setState({
      formErrors: fieldValidationErrors,
      emailValid: emailValid,
      passwordValid: passwordValid
      }, 
      this.validateForm
    );
  }

  validateForm() {
    this.setState({
      formValid: this.state.emailValid && this.state.passwordValid
    });
  }
  
  errorClass(error) {
    return(error.length === 0 ? '' : 'is-invalid');
  }

  render () {
    return (
      <form className="demoForm">
        <h2>Sign up</h2>
      
        <hr/>

        <div className="form-group">
          <label htmlFor="email">Email address</label>

          <input 
            type="email" 
            className={"form-control " + this.errorClass(this.state.formErrors.email)}
            name="email" 
            value={this.state.email} 
            onChange={this.handleUserInput} 
            />
        </div>

        <div className={"form-group " + this.errorClass(this.state.formErrors.password)}>
          <label htmlFor="password">Password</label>

          <input 
            type="password" 
            className={"form-control " + this.errorClass(this.state.formErrors.password)}
            name="password" 
            value={this.state.password} 
            onChange={this.handleUserInput} 
            />
        </div>

        <button 
          type="submit" 
          className="btn btn-primary"
          disabled={!this.state.formValid}
        >
          <span>Sign up</span>
        </button>

        <Errors formErrors={this.state.formErrors} />
      </form>
    )
  }
}

class Errors extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: props
    };
  }

  render () {
    let err = this.state.data.formErrors;
    let res = [];
    
    Object.keys(err).map((k,i)=>{
      if(err[k].length > 0) res.push(<li key={i}>{k} {err[k]}</li>);
    });

    if (res.length > 0) {      
      return (
        <div className='alert alert-danger mt-3'>
          <ul className="p-0 pl-2 m-0">
            {res}
          </ul>
        </div>
      );
    }

    return (<div></div>);
  }
}

export default Form;