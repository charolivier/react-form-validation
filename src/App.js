import React, { Component } from 'react';
import logo from './logo.svg';
import Form from './Form';

import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <div className="container py-5">
          <div className="row">
            <div className="col-sm-6 mx-auto">
              <div className="jumbotron text-left">
                <Form />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
